from django.db import models


class Village(models.Model):
    
    GREAT_SHINOBI_NATIONS = (
        ('L', 'Leaf Village'),
        ('S', 'Sand Village'),
        ('M', 'Mist Village'),
        ('C', 'Cloud Village'),
        ('S', 'Stone Village')
    )

    name  = models.CharField(max_length=1, choices=GREAT_SHINOBI_NATIONS)
    kage  = models.CharField(max_length=50)
    
    def __str__(self):
        return f"{self.name} - {self.kage}"

class People(models.Model):

    village       = models.ForeignKey(Village, on_delete=models.CASCADE, related_name='Village')
    name          = models.CharField(max_length=50)
    age           = models.IntegerField()
    special_jutsu = models.CharField(max_length=50, null=True) 

    def __str__(self):
        return f"{self.name} - {self.village.name}"