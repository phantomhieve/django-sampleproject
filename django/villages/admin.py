from django.contrib import admin
from .models import Village, People

admin.site.register([Village, People])
