from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from .serializers import PeopleSerializer, VillageSerializer
from .models import People, Village

class PeopleList(APIView):
    permission_classes = (IsAuthenticated,)
    """
    List all people, or create new snippet
    """
    def get(self, request):
        peoples    = People.objects.all()
        serializer = PeopleSerializer(peoples, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = PeopleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class VillageList(APIView):
    permission_classes = (IsAuthenticated,)
    """
    List all villages, or create new village
    """
    def get(self, request):
        village = Village.objects.all()
        serializer = VillageSerializer(village, many=True)
        return Response(serializer.data)
    
    def post(self, request):
        serializer = VillageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

