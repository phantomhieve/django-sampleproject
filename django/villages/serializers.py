from rest_framework import serializers
from .models import Village, People

class PeopleSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model  = People
        fields = '__all__'
    
class VillageSerializer(serializers.ModelSerializer):
    village = serializers.CharField(source='get_name_display', required=False)

    class Meta:
        model   = Village
        fields = '__all__'