from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt import views as jwt_views

from .views import PeopleList, VillageList

urlpatterns = [
    path('api/people', PeopleList.as_view()),
    path('api/village', VillageList.as_view()),
    path('api/token/', jwt_views.TokenObtainPairView.as_view()),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view()),
]
